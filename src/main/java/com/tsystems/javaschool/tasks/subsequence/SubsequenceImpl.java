package com.tsystems.javaschool.tasks.subsequence;

/**
 * Created by Roman N on 01.03.2018
 */
 
import java.util.List;

public class SubsequenceImpl implements Subsequence {

    @Override
    public boolean find(List x, List y) {
        boolean isCorrect = true;                                   //начальное значение флага
        int point = 0;                                              //вспомогательный счётчик
        for (int iterX = 0; iterX < x.size(); iterX++) {
            if (!isCorrect) break;
            isCorrect = false;
            for (int iterY = point; iterY < y.size(); iterY++) {
                if (x.get(iterX).equals(y.get(iterY))) {
                    isCorrect = true;
                    point = iterY + 1;
                    break;
                }
            }
        }
        return isCorrect;                                           //возвращаем значение
    }
}
