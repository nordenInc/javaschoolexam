package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;

public class App {

        public static void main(String[] args) {

            Subsequence seq = new SubsequenceImpl();
            boolean a = seq.find(Arrays.asList("A", "B", "C", "D"),
                    Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
            System.out.println(a);
 
        }
}