package com.tsystems.javaschool.tasks.calculator;
 
/**
 * Created by Roman N on 01.03.2018
 */

public class CalculatorImpl implements Calculator {

    private String stackList = ""; // переменная, содержащая элементы, попавшие в стек.
    private String list = ""; // переменная, содержащая элементы, попавшие в лист.
    private boolean correct = true; // переменная, предоставляющая доступ к вычислению выражения.
    int leftParenthesis = 0; // переменная, считывающая количество левых скобок.
    int rightParenthesis = 0; // переменная, считывающая количество правых скобок.

    public static void main(String[] args) {
        Calculator calculator = new CalculatorImpl();
        System.out.println("Результат: "+calculator.evaluate("(1+38)*4-5"));
    }

    private static String conversion(String str){
        String operandOne = ""; // переменная для первого операнда.
        String operandTwo = ""; // переменная для второго операнда.
        String updateString = ""; // переменная для записи временного значения в ходе решения выражения.
        double result = 0; // переменная, хранящая результат вычисления при выполнении программы.
        int count = 0; // переменная, которая указывает положение двух операндов.
        int N = str.length();
        int i = 0;
        while (i < N){
            if ((str.charAt(i) == '+' || str.charAt(i) == '-' || str.charAt(i) == '*' || str.charAt(i) == '/') && (str.charAt(i - 1) != ' ') ){
                /* получаем второй операнд */
                for (int j = i - 1; str.charAt(j) != (' '); j--){count = j;}
                for (int k = count; str.charAt(k) != str.charAt(i); k++){operandTwo += str.charAt(k);}
                count = count - 2;
                /* получаем первый операнд */
                for (int j = count; str.charAt(j) != ' '; j--){count = j;}
                for (int k = count; str.charAt(k) != ' '; k++){operandOne += str.charAt(k);}
                /* получаем промежуточный результат */
                if (str.charAt(i) == '+'){result = Double.parseDouble(operandOne) + Double.parseDouble(operandTwo);}
                if (str.charAt(i) == '-'){result = Double.parseDouble(operandOne) - Double.parseDouble(operandTwo);}
                if (str.charAt(i) == '*'){result = Double.parseDouble(operandOne) * Double.parseDouble(operandTwo);}
                if (str.charAt(i) == '/'){
                    if (Double.parseDouble(operandTwo) != 0){result = Double.parseDouble(operandOne) / Double.parseDouble(operandTwo);}
                    else {
                        str = "Infinity";
                        i = N;
                        continue;
                    }
                }
                operandOne = "";
                operandTwo = "";
                updateString = "";
                /* заменяем строку двух операндов, ближайших к оператору на строку с результатом, получаем новую строку */
                for (int k = 0; k < str.length(); k++){
                    if (k >= count && k <= i ){

                        if ((i + 1) == str.length()){updateString = Double.toString(result);
                            k = i;
                            continue;
                        }
                        if (str.charAt(i + 1) == ' ' || str.charAt(i + 1) == '+' || str.charAt(i + 1) == '-'
                                || str.charAt(i + 1) == '*' || str.charAt(i + 1) == '/'){
                            updateString=updateString.substring(0,k) + Double.toString(result) + updateString.substring(k,updateString.length());
                        } else {
                            updateString=updateString.substring(0,k) + Double.toString(result) + " " + updateString.substring(k,updateString.length());
                        }
                        k = i;
                    }
                    else {
                        updateString += str.charAt(k);
                    }
                }

                i = 0;
                str = updateString;
                N = str.length();
            }

            i++;
        }
        /* округляем результат */
        if (str.indexOf(".") + 4 < str.length() && (Double.parseDouble(str) < 10000000)){
            if ((Integer.parseInt(str.charAt(str.indexOf(".") + 4) + "") > 5)){
                str = Double.toString(Double.parseDouble(str) + 0.001).substring(0,str.indexOf(".") + 4);
            } else {str = str.substring(0,str.indexOf(".") + 4);}
        }

        return str.trim();
    }

    @Override
    public String evaluate(String statement) {
        for (int i = 0; i < statement.length(); i++){
            /* проверка на корректность введенных данных */
            if (statement.charAt(i) == '(') {leftParenthesis++;}
            if (statement.charAt(i) == ')') {rightParenthesis++;}
            if (statement.charAt(i) == '0' || statement.charAt(i) == '1' || statement.charAt(i) == '2' || statement.charAt(i) == '3' || statement.charAt(i) == '4'
                    || statement.charAt(i) == '5' || statement.charAt(i) == '6' || statement.charAt(i) == '7' || statement.charAt(i) == '8' || statement.charAt(i) == '9'
                    || (statement.charAt(i) == '*') || statement.charAt(i) == '/' || statement.charAt(i)=='+' || statement.charAt(i) == '-' || statement.charAt(i) == '('
                    || statement.charAt(i) == ')' || statement.charAt(i) == '.')
            {
                if ((statement.charAt(statement.length() - 1) == '*') || (statement.charAt(statement.length() - 1) == '/')
                        || (statement.charAt(statement.length() - 1) == '+') || (statement.charAt(statement.length() - 1) == '-')
                        || (statement.charAt(i) == '*' && i == 0) || (statement.charAt(i) == '/' && i == 0)
                        || (statement.charAt(0) == '+') || (statement.charAt(0) == ')') || (statement.charAt(statement.length() - 1) == '(')
                        || (statement.charAt(0) == '.') || (statement.charAt(statement.length() - 1) == '.')){
                    System.err.println("Некорректная запись, связана с границами");
                    correct = false;
                    i = statement.length();
                    continue;
                }
                if (statement.charAt(i) == '.'){
                    if (statement.charAt(i + 1) == '*' || statement.charAt(i + 1) == '/' || statement.charAt(i + 1) == '+' || statement.charAt(i + 1) == '-'
                            || statement.charAt(i + 1) == '.' || statement.charAt(i + 1) == '(' || statement.charAt(i + 1) == ')'){
                        System.err.println("Некорректная запись, связана с '.'");
                        correct = false;
                        i = statement.length();
                        continue;
                    }

                }
                if (statement.charAt(i) == '('){
                    if((statement.charAt(i + 1) != '0' && statement.charAt(i + 1) != '1' && statement.charAt(i + 1) != '2'
                            && statement.charAt(i + 1) != '3' && statement.charAt(i + 1) != '4' && statement.charAt(i + 1) != '5'
                            && statement.charAt(i + 1) != '6' && statement.charAt(i + 1) != '7' && statement.charAt(i + 1) != '8'
                            && statement.charAt(i + 1) != '9' && statement.charAt(i + 1) != '(' && statement.charAt(i + 1) != '-')){
                        System.err.println("Некорректная запись, связана с '('");
                        correct = false;
                        i = statement.length();
                        continue;
                    }
                }
                if (statement.charAt(i) == ')' && i != statement.length() - 1){
                    if ((statement.charAt(i - 1) != '0' && statement.charAt(i - 1) != '1' && statement.charAt(i - 1) != '2'
                            && statement.charAt(i - 1) != '3' && statement.charAt(i - 1) != '4' && statement.charAt(i - 1) != '5'
                            && statement.charAt(i - 1) != '6' && statement.charAt(i - 1) != '7' && statement.charAt(i - 1) != '8'
                            && statement.charAt(i - 1) != '9' && statement.charAt(i - 1) != ')')
                            || (statement.charAt(i + 1) != '*' && statement.charAt(i + 1) != '/' && statement.charAt(i + 1) != '+'
                            && statement.charAt(i + 1) != '-' && statement.charAt(i + 1) != ')')){
                        System.err.println("Некорректная запись связана с ) ");
                        correct = false;
                        i = statement.length();
                        continue;
                    }
                }
                if (statement.charAt(i) == '*' || statement.charAt(i) == '/' || statement.charAt(i) == '+' || statement.charAt(i) == '-'){
                    if ((statement.charAt(i + 1) != '0') && (statement.charAt(i + 1) != '1') && (statement.charAt(i + 1) != '2')
                            && (statement.charAt(i + 1) != '3') && (statement.charAt(i + 1) != '4') && (statement.charAt(i + 1) != '5')
                            && (statement.charAt(i + 1) != '6') && (statement.charAt(i + 1) != '7') && (statement.charAt(i + 1) != '8')
                            && (statement.charAt(i + 1) != '9') && (statement.charAt(i + 1) != '(')) {
                        System.err.println("Некорректная запись связана с '*,/,+,-'");
                        correct = false;
                        i = statement.length();
                        continue;
                    }
                }

            } else {correct = false; i = statement.length();}
        }

        if (leftParenthesis != rightParenthesis ||correct == false)
        {
            statement = "null";
        } else {

            /*Т.к проверка прошла успешно, то начинаем наше выражение переводить в постфиксную форму, используя list и stackList*/
            for (int i = 0; i < statement.length(); i++){

                if (statement.charAt(0) != '(' && statement.charAt(0) != ' '){
                    statement = " " + statement;
                    continue;
                }
                if (statement.charAt(i) == '(' || statement.charAt(i) == ')' ||
                        statement.charAt(i) == '*' || statement.charAt(i) == '/' ||
                        statement.charAt(i) == '+' || statement.charAt(i) == '-' ){

                    if (statement.charAt(i) == '-' && (statement.charAt(i - 1) == ' ' || statement.charAt(i - 1) == '(')){
                        list += " " + statement.charAt(i);
                        continue;
                    }

                    if (stackList.length() == 0 || statement.charAt(i) == '(' ){
                        stackList += statement.charAt(i);
                        continue;
                    }

                    if (statement.charAt(i) == ')'){
                        int j = stackList.length() - 1;
                        while (j > stackList.lastIndexOf('(')){
                            list += stackList.charAt(j);
                            j--;
                        }
                        stackList = stackList.substring(0,stackList.lastIndexOf('('));
                        continue;
                    }

                    if ((statement.charAt(i) == '+' || statement.charAt(i) == '-') && (stackList.length() != 0)) {

                        if (stackList.charAt(stackList.length() - 1) == '('){
                            stackList += statement.charAt(i);
                            continue;
                        }

                        if (stackList.charAt(stackList.length() - 1) == '+' || stackList.charAt(stackList.length() - 1) == '-'
                                || stackList.charAt(stackList.length() - 1) == '*' || stackList.charAt(stackList.length() - 1) == '/'){
                            int j = stackList.length() - 1;

                            if (stackList.lastIndexOf('(') >= 0){
                                while (j > stackList.lastIndexOf('(')){
                                    list += stackList.charAt(j);
                                    j--;
                                }
                                stackList = stackList.substring(0,stackList.lastIndexOf('(')+1);
                                stackList += statement.charAt(i);

                            } else {
                                while (j >= 0){
                                    list += stackList.charAt(j);
                                    j--;
                                }
                                stackList = "";
                                stackList += statement.charAt(i);
                            }
                            continue;
                        }
                    }
                    if ((statement.charAt(i) == '*' || statement.charAt(i) == '/') && (stackList.length() != 0)) {

                        if (stackList.charAt(stackList.length() - 1) == '(' || stackList.charAt(stackList.length() - 1) == '+' ||
                                stackList.charAt(stackList.length() - 1) == '-'){
                            stackList += statement.charAt(i);
                            continue;
                        }
                        if (stackList.charAt(stackList.length() - 1) == '*' || stackList.charAt(stackList.length() - 1) == '/'){
                            int j = stackList.length() - 1;

                            if (stackList.lastIndexOf('(') >= 0){
                                while (j > stackList.lastIndexOf('(')){
                                    if (stackList.charAt(j) != '+' || stackList.charAt(j) != '-'){
                                        list += stackList.charAt(j);
                                        j--;
                                    } else {break;}
                                }

                                if ((stackList.lastIndexOf("+") >= 0) && stackList.lastIndexOf("+") > stackList.lastIndexOf('(')) {
                                    stackList = stackList.substring(0,stackList.lastIndexOf('+') + 1);}
                                else if ((stackList.lastIndexOf("-") >= 0) && stackList.lastIndexOf("-") > stackList.lastIndexOf('(')) {
                                    stackList = stackList.substring(0,stackList.lastIndexOf('-') + 1);}
                                else {stackList = stackList.substring(0,stackList.lastIndexOf('(') + 1);}
                                stackList += statement.charAt(i);

                            } else {
                                while (j >= 0){
                                    if (stackList.charAt(j) != '+' || stackList.charAt(j) != '-'){
                                        list += stackList.charAt(j);
                                        j--;
                                    } else {break;}
                                }

                                if (stackList.lastIndexOf("+") >= 0) {
                                    stackList = stackList.substring(0,stackList.lastIndexOf('+') + 1);
                                } if (stackList.lastIndexOf("-") >= 0) {
                                    stackList = stackList.substring(0,stackList.lastIndexOf('-') + 1);}
                                else {stackList = "";}

                                stackList += statement.charAt(i);

                            }
                            continue;
                        }

                    }

                } else if (statement.charAt(i) == '0' || statement.charAt(i) == '1' || statement.charAt(i) == '2' || statement.charAt(i) == '3'
                        || statement.charAt(i) == '4' || statement.charAt(i) == '5' || statement.charAt(i) == '6'
                        || statement.charAt(i) == '7' || statement.charAt(i) == '8' || statement.charAt(i) == '9' || statement.charAt(i) == '.'){


                    if (statement.charAt(i - 1) == '+' || (statement.charAt(i - 1) == '-' && (statement.charAt(i - 2) != ' ' && statement.charAt(i - 2) != '(')) || statement.charAt(i - 1) == '*' || statement.charAt(i - 1) == '/' || statement.charAt(i - 1) == '('){
                        list += " ";
                    }
                    list += statement.charAt(i);


                }
            }

            for (int k = stackList.length() - 1; k >= 0; k--){
                list += stackList.charAt(k);
            }
            /* используем conversion(list) для получение результата */
            statement = conversion(list);
        }
        //System.out.println(list);

        return statement;
    }
}